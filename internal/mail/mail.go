package mail

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"time"

	imap "github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"

	log "github.com/sirupsen/logrus"

	"gitlab.com/yakshaving.art/mqil/internal"
	"gitlab.com/yakshaving.art/mqil/internal/metrics"
)

// Client is a client that handles the communication with the IMAP service
type Client struct {
	inbox string

	imapClient *client.Client
}

// New creates a new MailClient
func New(inbox, server, user, password string) (*Client, error) {
	log.Debugf("Dialing to remote server %s", server)
	imapClient, err := client.DialTLS(server, nil)
	if err != nil {
		return nil, fmt.Errorf("could not connect to mail server %s: %s", server, err)
	}

	log.Debugf("Connection acquired, logging in to remote server %s", server)
	err = imapClient.Login(user, password)
	if err != nil {
		return nil, fmt.Errorf("failed to login to the remote server %s with the provided credentials: %s",
			server, err)
	}

	return &Client{
		inbox:      inbox,
		imapClient: imapClient,
	}, nil
}

// Fetch fetches emails and dumps them to stdout for now
func (c *Client) Fetch(matchers []internal.Matcher, ch chan internal.Executor) error {
	defer close(ch)

	metrics.CheckedEmails.Inc()

	if len(matchers) == 0 {
		return fmt.Errorf("matchers list is empty, nothing will be matched")
	}

	mbox, err := c.imapClient.Select(c.inbox, false)
	if err != nil {
		return fmt.Errorf("could not fetch INBOX: %s", err)
	}
	log.Debugf("Mailbox: %#v", mbox)

	if mbox.Messages == 0 {
		log.Debug("No messages found")
		return nil
	}

	// How many to read
	seqSet := new(imap.SeqSet)
	seqSet.AddRange(1, mbox.Messages) // All of them

	// Get the whole message body
	var section imap.BodySectionName
	items := []imap.FetchItem{section.FetchItem()}

	messages := make(chan *imap.Message, 1)
	go func() {
		if err := c.imapClient.Fetch(seqSet, items, messages); err != nil {
			log.Fatal(err)
		}
	}()

	for msg := range messages {
		if msg == nil {
			return fmt.Errorf("Server didn't return a message")
		}

		r := msg.GetBody(&section)
		if r == nil {
			return fmt.Errorf("Server didn't return a message body")
		}

		log.Debugf("Reading message sequence %d", msg.SeqNum)
		mr, err := mail.CreateReader(r)
		if err != nil {
			return fmt.Errorf("could not create reader for email %d: %s", msg.SeqNum, err)
		}

		var date time.Time
		var to []internal.EmailAddress
		var from internal.EmailAddress
		var subject string

		header := mr.Header
		if date, err = header.Date(); err != nil {
			return fmt.Errorf("failed to read Date from email %d: %s", msg.SeqNum, err)
		}
		if subject, err = header.Subject(); err != nil {
			return fmt.Errorf("failed to read Subject from email %d: %s", msg.SeqNum, err)
		}

		if fromAddresses, err := header.AddressList("From"); err != nil {
			return fmt.Errorf("failed to read From address from email %d: %s", msg.SeqNum, err)
		} else {
			for _, address := range fromAddresses {
				from = internal.EmailAddress{
					Name:    address.Name,
					Address: address.Address,
				}
			}
		}

		if toAddress, err := header.AddressList("To"); err != nil {
			return fmt.Errorf("failed to read To address list from email %d: %s", msg.SeqNum, err)
		} else {
			to = make([]internal.EmailAddress, len(toAddress))
			for i, address := range toAddress {
				to[i] = internal.EmailAddress{
					Name:    address.Name,
					Address: address.Address,
				}
			}
		}

		emailHeader := internal.EmailHeader{
			Date:    date,
			To:      to,
			From:    from,
			Subject: subject,
		}
		metrics.ReceivedEmails.Inc()

		for _, m := range matchers {
			if m.Match(emailHeader) {
				log.Debugf("Emails %s matched, loading payload and then sending to channel", emailHeader)

				emailPayload, err := c.loadEmailPayload(mr)
				if err != nil {
					return fmt.Errorf("failed to load payload of email %s: %s", emailHeader, err)
				}
				ch <- m.CreateExecutor(emailHeader, emailPayload, func() error {
					seqset := new(imap.SeqSet)
					seqset.AddNum(msg.SeqNum)
					item := imap.FormatFlagsOp(imap.AddFlags, false)
					flags := []interface{}{imap.DeletedFlag}

					log.Debugf("Deleting email %#v", seqset)

					if err := c.imapClient.Store(seqset, item, flags, nil); err != nil {
						return fmt.Errorf("failed to flag email %d as deleted: %s", msg.SeqNum, err)
					}
					return c.imapClient.Expunge(nil)
				})

			}
		}
		metrics.UnmatchedEmails.Inc()
	}
	return nil
}

func (c *Client) loadEmailPayload(mr *mail.Reader) (internal.EmailPayload, error) {
	var body string
	attachments := make([]internal.EmailAttachment, 0)

	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		} else if err != nil {
			return internal.EmailPayload{}, fmt.Errorf("failed to read multipart: %s", err)
		}

		switch h := p.Header.(type) {
		case mail.TextHeader:
			if strings.HasPrefix(p.Header.Get("Content-Type"), "text/plain") {
				b, err := ioutil.ReadAll(p.Body)
				if err != nil {
					return internal.EmailPayload{}, fmt.Errorf("failed to read body: %s",
						err)
				}
				body = string(b)
			}

		case mail.AttachmentHeader: // This is an attachment
			filename, err := h.Filename()
			if err != nil {
				return internal.EmailPayload{}, fmt.Errorf("failed to read attachment filename: %s", err)
			}

			b, err := ioutil.ReadAll(p.Body)
			if err != nil {
				return internal.EmailPayload{}, fmt.Errorf("failed to read attachment  %s: %s",
					filename, err)
			}

			tmpfile, err := ioutil.TempFile("", fmt.Sprintf("attachment.*.%s", filename))
			if err != nil {
				return internal.EmailPayload{}, fmt.Errorf("failed to create attachment temporary file %s: %s",
					filename, err)
			}

			if _, err = tmpfile.Write(b); err != nil {
				defer os.Remove(tmpfile.Name())
				return internal.EmailPayload{}, fmt.Errorf("failed to write to temporary file %s: %s",
					tmpfile.Name(), err)

			}

			if err := tmpfile.Close(); err != nil {
				defer os.Remove(tmpfile.Name())
				return internal.EmailPayload{}, fmt.Errorf("failed to close temporary file %s: %s",
					tmpfile.Name(), err)
			}

			attachments = append(attachments, internal.EmailAttachment{
				Name:     filename,
				Filepath: tmpfile.Name(),
			})
		}
	}

	return internal.EmailPayload{
		Body:        body,
		Attachments: attachments,
	}, nil
}
