package metrics_test

import (
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/yakshaving.art/mqil/internal/metrics"
)

func TestListening(t *testing.T) {
	a := assert.New(t)
	go func() {
		metrics.Serve("localhost:1234", "/metrics")
	}()

	metrics.ProcessedEmails.WithLabelValues("dryrun", "success").Inc()
	metrics.ReceivedEmails.Inc()

	r, err := http.Get("http://localhost:1234/metrics")
	a.NoError(err)

	defer r.Body.Close()
	b, err := ioutil.ReadAll(r.Body)
	a.NoError(err)

	a.Contains(string(b), "mqil_emails_received_total 1")
}
