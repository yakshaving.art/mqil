package metrics

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// Prometheus Metrics
var (
	namespace = "mqil"

	bootTime = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "boot_time_seconds",
		Help:      "unix timestamp of when the service was started",
	})
	CheckedEmails = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: "emails",
		Name:      "checked_total",
		Help:      "Total number of times mqil checked emails",
	})
	ReceivedEmails = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: "emails",
		Name:      "received_total",
		Help:      "Total number of received emails",
	})
	ProcessedEmails = promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: "emails",
		Name:      "processed_total",
		Help:      "Total number of emails processed",
	}, []string{"script", "status"})
	UnmatchedEmails = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: "emails",
		Name:      "unmatched_total",
		Help:      "Total number of emails that did not match any matcher",
	})
	NukedUnsolicitedEmails = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Subsystem: "emails",
		Name:      "nuked_onsolicited_total",
		Help:      "Total number of emails that did not match any matcher and were nuked",
	})
)

// Serve registers the metrics handler and starts the listener on the given address and path
func Serve(address, path string) error {
	bootTime.Set(float64(time.Now().Unix()))

	http.Handle(path, promhttp.Handler())
	return http.ListenAndServe(address, nil)
}
