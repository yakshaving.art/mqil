package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/yakshaving.art/mqil/internal"
	"gitlab.com/yakshaving.art/mqil/internal/config"
)

func TestLoadingValidConfiguration(t *testing.T) {
	a := assert.New(t)

	c, err := config.Load("config-fixture.yml")
	a.NoError(err, "failed to load the configuration")

	a.EqualValues(internal.Config{
		Mappings: []internal.ScriptMapping{
			{
				Subject: ".*",
				From:    ".*",
				To:      "dorothy+chainsaw@example.org",
				Retry:   3,
				Script:  "chainsaw_script.sh",
				Args: []string{
					"{{ .To }}",
					"{{ .From }}",
					"{{ .Subject }}",
					"{{ .Body }}",
					"{{ .Attachments }}"},
			},
		},
	}, c)
}

func TestLoadingNonExistingConfigFileFails(t *testing.T) {
	a := assert.New(t)

	_, err := config.Load("non-existing-config-fixture.yml")
	a.EqualError(err, "failed to read configuration file non-existing-config-fixture.yml: "+
		"open non-existing-config-fixture.yml: no such file or directory")
}

func TestLoadingInvalidConfigFileFails(t *testing.T) {
	a := assert.New(t)

	_, err := config.Load("invalid-config-fixture.yml")
	a.EqualError(err, "failed to parse configuration file invalid-config-fixture.yml: "+
		"yaml: unmarshal errors:\n  line 14: "+
		"field invalid_field not found in type internal.ScriptMapping")
}
