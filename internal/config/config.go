package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"

	"gitlab.com/yakshaving.art/mqil/internal"
)

// Load loads a configuration from the passed filename
func Load(filename string) (internal.Config, error) {
	var c internal.Config

	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return c, fmt.Errorf("failed to read configuration file %s: %s", filename, err)
	}

	if err = yaml.UnmarshalStrict(b, &c); err != nil {
		return c, fmt.Errorf("failed to parse configuration file %s: %s", filename, err)
	}

	return c, nil
}
