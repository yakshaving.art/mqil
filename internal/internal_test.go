package internal_test

import (
	"io/ioutil"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/yakshaving.art/mqil/internal"
)

func TestEmailHeader(t *testing.T) {
	d, err := time.Parse(time.RFC3339, "2006-01-02T15:04:05Z")
	assert.NoError(t, err)

	h := internal.EmailHeader{
		Date: d,
		From: internal.EmailAddress{
			Name:    "someone",
			Address: "someone@gmail.com",
		},
		To: []internal.EmailAddress{
			{
				Name:    "someone else",
				Address: "someoneelse@gmail.com",
			},
		},
		Subject: "just a test email",
	}
	assert.Equal(t, "4c9a89f6b3b4b4d0ec14275ed2541fb6", h.Hash())
	assert.Equal(t, "Email sent on 2006-01-02T15:04:05Z from someone <someone@gmail.com>"+
		" to someone else <someoneelse@gmail.com> with subject 'just a test email'", h.String())
}

func TestEmailPayload(t *testing.T) {
	a := assert.New(t)

	f, err := ioutil.TempFile("", "somefile")
	a.NoError(err)

	p := internal.EmailPayload{
		Attachments: []internal.EmailAttachment{
			{
				Name:     "something",
				Filepath: f.Name(),
			},
		},
	}
	a.Contains(p.String(), "Body:\n\nAttachments:\n  something")
	p.Cleanup()
}
