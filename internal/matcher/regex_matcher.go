package matcher

import (
	"bytes"
	"fmt"
	"os/exec"
	"regexp"
	"text/template"

	log "github.com/sirupsen/logrus"

	"gitlab.com/yakshaving.art/mqil/internal"
	"gitlab.com/yakshaving.art/mqil/internal/metrics"
)

// Args holds all the arguments necessary for a creating and running a matcher
type Args struct {
	Mapping internal.ScriptMapping
	DryRun  bool
	Counter internal.RetryCounter
}

// NewRegexMatcher creates a new Regex Matcher
func NewRegexMatcher(m internal.ScriptMapping, counter internal.RetryCounter, dryRun bool) internal.Matcher {
	args := newScriptArgs(m.Args)
	return regexMatcher{
		mapping: m,

		subjectMatcher: regexp.MustCompile(m.Subject),
		toMatcher:      regexp.MustCompile(m.To),
		fromMatcher:    regexp.MustCompile(m.From),

		creator: func(h internal.EmailHeader, p internal.EmailPayload, deleter func() error) internal.Executor {
			d := scriptData{
				executable: m.Script,
				payload:    emailPayload{h, p},
				args:       args,
			}
			if dryRun {
				return dryRunExecutor{d}
			}
			return scriptExecutor{
				data:    d,
				counter: counter,
				retry:   m.Retry,
				deleter: deleter,
			}
		},
	}
}

// NewNukeAllMatcher creates a new matcher that nukes everything it gets fed in
func NewNukeAllMatcher() internal.Matcher {
	return nukeAllMatcher{}
}

type regexMatcher struct {
	mapping internal.ScriptMapping
	creator func(internal.EmailHeader, internal.EmailPayload, func() error) internal.Executor

	subjectMatcher *regexp.Regexp
	toMatcher      *regexp.Regexp
	fromMatcher    *regexp.Regexp
}

// Match implements matcher.Match
func (m regexMatcher) Match(h internal.EmailHeader) bool {
	for _, t := range h.To {
		if !m.toMatcher.MatchString(t.Address) {
			return false
		}
	}

	return m.subjectMatcher.MatchString(h.Subject) &&
		m.fromMatcher.MatchString(h.From.Address)
}

// CreateExecutor implements matcher.CreateExecutor
func (m regexMatcher) CreateExecutor(h internal.EmailHeader, p internal.EmailPayload, deleter func() error) internal.Executor {
	return m.creator(h, p, deleter)
}

// emailPayload is the struct used to send the whole email data to the script executor
type emailPayload struct {
	Header  internal.EmailHeader
	Payload internal.EmailPayload
}

func (e emailPayload) String() string {
	return fmt.Sprintf("Header: %s\nPayload: %s", e.Header, e.Payload)
}

func (e emailPayload) Cleanup() {
	e.Payload.Cleanup()
}

type scriptData struct {
	executable string
	args       scriptArgs

	payload emailPayload
}

type scriptExecutor struct {
	data    scriptData
	retry   int
	counter internal.RetryCounter
	deleter func() error
}

// Execute implements Executor.Execute
func (d scriptExecutor) Execute() error {
	defer d.data.payload.Cleanup()

	emailHash := d.data.payload.Header.Hash()
	current, err := d.counter.Current(emailHash)
	if err != nil {
		return fmt.Errorf("could not retrieve current counter: %s", err)
	}
	if current > uint32(d.retry) {
		log.Infof(
			"Email %s has not been executed because it's over the retry count %d/%d",
			d.data.payload.Header,
			current,
			d.retry)
		return nil
	}
	if err = d.counter.Inc(emailHash); err != nil {
		return fmt.Errorf("could not increment counter: %s", err)
	}

	args := d.data.args.Args(d.data.payload)

	log.Debugf("Execute %s %#v\n%s", d.data.executable, args, d.data.payload)

	cmd := exec.Command(d.data.executable, args...)
	s, err := cmd.CombinedOutput()
	log.Infof("Script output:")
	log.Infof(string(s))
	if err == nil {
		if err = d.deleter(); err != nil {
			metrics.ProcessedEmails.WithLabelValues(d.data.executable, "failed-delete").Inc()
		} else {
			metrics.ProcessedEmails.WithLabelValues(d.data.executable, "successful").Inc()
		}
	} else {
		metrics.ProcessedEmails.WithLabelValues(d.data.executable, "failed").Inc()
	}
	return err
}

// dryRunExecutor does nothing
type dryRunExecutor struct {
	data scriptData
}

// Execute implements Executor.Execute
func (d dryRunExecutor) Execute() error {
	defer d.data.payload.Cleanup()

	log.Infof("DryRun %s %#v\n%s", d.data.executable, d.data.args.Args(d.data.payload), d.data.payload)
	metrics.ProcessedEmails.WithLabelValues(d.data.executable, "dryrun").Inc()
	return nil
}

type scriptArgs struct {
	args []*template.Template
}

func newScriptArgs(args []string) scriptArgs {
	a := make([]*template.Template, len(args))
	for i, t := range args {
		a[i] = template.Must(template.New("tmplt").Parse(t))
	}

	return scriptArgs{
		args: a,
	}
}

func (s scriptArgs) Args(p emailPayload) []string {
	args := make([]string, len(s.args))
	for i, a := range s.args {
		b := bytes.NewBufferString("")
		a.Execute(b, p)
		args[i] = b.String()
	}
	return args
}

type nukeAllMatcher struct{}

func (nukeAllMatcher) Match(h internal.EmailHeader) bool {
	return true
}

func (nukeAllMatcher) CreateExecutor(_ internal.EmailHeader, _ internal.EmailPayload, deleter func() error) internal.Executor {
	return nukeAllExecutor{deleter}
}

type nukeAllExecutor struct {
	deleter func() error
}

func (d nukeAllExecutor) Execute() error {
	metrics.NukedUnsolicitedEmails.Inc()
	return d.deleter()
}
