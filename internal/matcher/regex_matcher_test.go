package matcher_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/yakshaving.art/mqil/internal"
	"gitlab.com/yakshaving.art/mqil/internal/matcher"
)

func Test_RegexMatcher(t *testing.T) {

	tt := []struct {
		name    string
		matcher internal.Matcher
		header  internal.EmailHeader
		payload internal.EmailPayload
		match   bool
	}{
		{
			name: "from is same domain as to",
			matcher: matcher.NewRegexMatcher(internal.ScriptMapping{
				To:    ".+@somewhere.com",
				From:  ".+@somewhere.com",
				Retry: 3,
			}, RetryCountMock{}, true),
			header: internal.EmailHeader{
				To: []internal.EmailAddress{
					{
						Name:    "Someone",
						Address: "someone@somewhere.com",
					},
				},
				From: internal.EmailAddress{
					Name:    "Someone Gmail",
					Address: "someone@gmail.com",
				},
			},
			payload: internal.EmailPayload{},
			match:   false,
		},
		{
			name: "a second address to is wrong",
			matcher: matcher.NewRegexMatcher(internal.ScriptMapping{
				To:    ".+@somewhere.com",
				From:  ".+@somewhere.com",
				Retry: 3,
			}, RetryCountMock{}, true),
			header: internal.EmailHeader{
				To: []internal.EmailAddress{
					{
						Name:    "Someone",
						Address: "someone@somewhere.com",
					},
					{
						Name:    "Someone Else at gmail",
						Address: "someone@gmail.com",
					},
				},
				From: internal.EmailAddress{
					Name:    "Someone Gmail",
					Address: "someone@gmail.com",
				},
			},
			payload: internal.EmailPayload{},
			match:   false,
		},
		{
			name: "to is same domain as from",
			matcher: matcher.NewRegexMatcher(internal.ScriptMapping{
				To:    ".+@somewhere.com",
				From:  ".+@somewhere.com",
				Retry: 3,
			}, RetryCountMock{}, true),
			header: internal.EmailHeader{
				To: []internal.EmailAddress{
					{
						Name:    "Someone Gmail",
						Address: "someone@gmail.com",
					},
				},
				From: internal.EmailAddress{
					Name:    "Someone",
					Address: "someone@somewhere.com",
				},
			},
			payload: internal.EmailPayload{},
			match:   false,
		},
		{
			name: "subject is not as expected",
			matcher: matcher.NewRegexMatcher(internal.ScriptMapping{
				Subject: "^expected subject$",
				To:      ".+@somewhere.com",
				From:    ".+@somewhere.com",
			}, RetryCountMock{}, true),
			header: internal.EmailHeader{
				Subject: "not expected subject",
				To: []internal.EmailAddress{
					{
						Name:    "Someone",
						Address: "someone@somewhere.com",
					},
				},
				From: internal.EmailAddress{
					Name:    "Someone",
					Address: "someone@somewhere.com",
				},
			},
			payload: internal.EmailPayload{},
			match:   false,
		},
		{
			name: "subject is not as expected",
			matcher: matcher.NewRegexMatcher(internal.ScriptMapping{
				Subject: "^expected subject$",
				To:      ".+@somewhere.com",
				From:    ".+@somewhere.com",
				Retry:   3,
			}, RetryCountMock{}, true),
			header: internal.EmailHeader{
				Subject: "expected subject",
				To: []internal.EmailAddress{
					{
						Name:    "Someone",
						Address: "someone@somewhere.com",
					},
				},
				From: internal.EmailAddress{
					Name:    "Someone",
					Address: "someone@somewhere.com",
				},
			},
			payload: internal.EmailPayload{},
			match:   true,
		},
		{
			name: "run script for real",
			matcher: matcher.NewRegexMatcher(internal.ScriptMapping{
				Subject: "^expected subject$",
				Script:  "echo",
				Args:    []string{`\n\nsubject: "{{ .Header.Subject }}"`}, // `{{ printf "%#v" . }}`,
				Retry:   3,
			}, RetryCountMock{}, false),
			header: internal.EmailHeader{
				Subject: "expected subject",
				To: []internal.EmailAddress{
					{
						Name:    "Someone",
						Address: "someone@somewhere.com",
					},
				},
				From: internal.EmailAddress{
					Name:    "Someone",
					Address: "someone@somewhere.com",
				},
			},
			payload: internal.EmailPayload{
				Body: "this is the body",
			},
			match: true,
		},
		{
			name:    "test matchall",
			matcher: matcher.NewNukeAllMatcher(),
			header:  internal.EmailHeader{},
			payload: internal.EmailPayload{},
			match:   true,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			a := assert.New(t)
			a.Equal(tc.match, tc.matcher.Match(tc.header), tc.name)

			c := tc.matcher.CreateExecutor(tc.header, tc.payload, func() error {
				return nil
			})
			err := c.Execute()
			a.NoError(err)
		})
	}
}

type RetryCountMock struct{}

func (RetryCountMock) Current(_ string) (uint32, error) {
	return 1, nil
}

func (RetryCountMock) Inc(_ string) error {
	return nil
}

func (RetryCountMock) Close() error {
	return nil
}
