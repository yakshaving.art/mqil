package db_test

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/yakshaving.art/mqil/internal/db"
)

func TestIncrementing(t *testing.T) {
	a := assert.New(t)

	f, err := ioutil.TempFile("", "boltdb")
	a.NoError(err)

	defer os.Remove(f.Name())

	d, err := db.Open(f.Name())
	a.NoError(err)

	defer d.Close()

	i, err := d.Current("my-email-hash")
	a.NoError(err)
	a.Equal(uint32(0), i)

	err = d.Inc("my-email-hash")
	a.NoError(err)

	i, err = d.Current("my-email-hash")
	a.NoError(err)
	a.Equal(uint32(1), i)

	err = d.Inc("my-email-hash")
	a.NoError(err)

	i, err = d.Current("my-email-hash")
	a.NoError(err)
	a.Equal(uint32(2), i)
}
