package db

import (
	"encoding/binary"
	"fmt"
	"sync"

	bolt "github.com/coreos/bbolt"

	"gitlab.com/yakshaving.art/mqil/internal"
)

var valueKey = []byte("value")

type incrementer struct {
	db   *bolt.DB
	lock *sync.Mutex
}

// Open creates a new DB object
func Open(filename string) (internal.RetryCounter, error) {
	db, err := bolt.Open(filename, 0600, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to open file %s: %s", filename, err)
	}

	return &incrementer{
		db:   db,
		lock: &sync.Mutex{},
	}, err
}

// Current returns the current value
func (d incrementer) Current(name string) (uint32, error) {
	d.lock.Lock()
	defer d.lock.Unlock()

	var i uint32
	err := d.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(name))
		if b == nil {
			return nil
		}

		value := b.Get(valueKey)
		if value != nil {
			i = binary.BigEndian.Uint32(value)
		}
		return nil
	})
	return i, err
}

// Inc increments one in the given bucket and returns the current number
func (d incrementer) Inc(name string) error {
	d.lock.Lock()
	defer d.lock.Unlock()

	var i uint32
	return d.db.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte(name))
		if err != nil {
			return fmt.Errorf("failed to create bucket %s: %s", name, err)
		}

		buffer := make([]byte, 4)
		value := b.Get(valueKey)
		if value != nil {
			i = binary.BigEndian.Uint32(value)
		}
		i++

		binary.BigEndian.PutUint32(buffer, i)

		b.Put(valueKey, buffer)
		if err != nil {
			return fmt.Errorf("failed to write value: %s", err)
		}
		return nil
	})
}

// Close closes the database file
func (d incrementer) Close() error {
	return d.db.Close()
}
