package internal

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

// EmailHeader is a parsed email with a text body
type EmailHeader struct {
	Date    time.Time
	From    EmailAddress
	To      []EmailAddress
	Subject string
}

func (eh EmailHeader) String() string {
	to := make([]string, len(eh.To))
	for i, a := range eh.To {
		to[i] = a.String()
	}
	return fmt.Sprintf("Email sent on %s from %s to %s with subject '%s'",
		eh.Date.Format(time.RFC3339), eh.From, strings.Join(to, ", "), eh.Subject)
}

// Hash returns the hash of an email header
func (eh EmailHeader) Hash() string {
	b := bytes.NewBufferString(eh.Date.Format(time.RFC3339))
	b.WriteString(eh.Subject)
	b.WriteString(eh.From.String())
	for _, a := range eh.To {
		b.WriteString(a.String())
	}

	h := md5.New()
	h.Write(b.Bytes())

	return hex.EncodeToString(h.Sum(nil))
}

// EmailAddress represents an address by splitting the name from the actual email address
type EmailAddress struct {
	Name    string
	Address string
}

func (ea EmailAddress) String() string {
	return strings.TrimSpace(fmt.Sprintf("%s <%s>", ea.Name, ea.Address))
}

// EmailPayload is the body and attachments that are shipped in an email
type EmailPayload struct {
	Body string

	// Attachments is a list of temporary files that were sent with the email
	Attachments []EmailAttachment
}

func (e EmailPayload) String() string {
	attachments := make([]string, len(e.Attachments))
	for i, a := range e.Attachments {
		attachments[i] = a.String()
	}
	return strings.TrimSpace(fmt.Sprintf("Body:\n%s\nAttachments:\n  %s",
		e.Body, strings.Join(attachments, "\n  ")))
}

// Cleanup is used to delete all the attachment files included in the payload
func (e EmailPayload) Cleanup() error {
	for _, a := range e.Attachments {
		log.Debugf("Deleting attachment %s - %s", a.Name, a.Filepath)
		if err := os.Remove(a.Filepath); err != nil {
			return err
		}
	}
	return nil
}

// EmailAttachment an email file attachment
type EmailAttachment struct {
	Name     string
	Filepath string
}

func (ea EmailAttachment) String() string {
	return strings.TrimSpace(fmt.Sprintf("%s (%s)", ea.Name, ea.Filepath))
}

// Matcher receives an EmailHeader and matches it or not
type Matcher interface {
	Match(EmailHeader) bool
	CreateExecutor(EmailHeader, EmailPayload, func() error) Executor
}

// Executor is the thing that knows how to execute an email script
type Executor interface {
	Execute() error
}

// Config is the configuration structure that is loaded from the config file
type Config struct {
	Mappings []ScriptMapping `yaml:"mappings"`
}

// ScriptMapping is one script mapped to a matcher
type ScriptMapping struct {
	Subject string   `yaml:"subject"`
	From    string   `yaml:"from"`
	To      string   `yaml:"to"`
	Retry   int      `yaml:"retry"`
	Script  string   `yaml:"script"`
	Args    []string `yaml:"args,omitempty"`
}

// RetryCounter allows to get current retry count and increment
type RetryCounter interface {
	Inc(name string) error
	Current(name string) (uint32, error)
	Close() error
}
