module gitlab.com/yakshaving.art/mqil

go 1.12

require (
	github.com/coreos/bbolt v1.3.0
	github.com/emersion/go-imap v1.0.0-beta.4
	github.com/emersion/go-message v0.9.2
	github.com/prometheus/client_golang v0.9.2
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/testify v1.2.2
	gopkg.in/yaml.v2 v2.2.2
)
