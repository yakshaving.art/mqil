# MQIL

From the creators of `how the fuck do you pronounce that`:

MQIL - the messaging queue over email that noone was asking for.

## Usage

`mqil [options]`

### Options

- **-config string** configuration file to load mappings from ("mqil.yml" by
    default)
- **-debug** enable debug level logging
- **-polling-interval-seconds integer** number of seconds between checking
    email polling intervals (60 by default)
- **-metrics-path string** path in which prometheus metrics are presented
    ("/metrics" by default)
- **-metrics-address string** address in which prometheus metrics are presented
    (":9097" by default)
- **-database filename** database filename used for email accounting
    ("mqil.bolt" by default)
- **-nuke-unsolicited** nuke emails that don't match any filter (handle with
    care)

### Required Environment Variables

- **MQIL_SERVER_ADDRESS** address of the imap server, for example:
    `mail.example.org:993`
- **MQIL_USERNAME** username used to authenticate to the server
- **MQIL_PASSWORD** password used to authenticate to the server

## Configuration Sample

```yaml
mappings:
  - subject: ".*" # optional, match all by default
    from: ".*" # optional, match all by default
    to: "dorothy+chainsaw@example.org" # optional, match all by default
    retry: 3 # optional, how many times to retry processing the message, 0 by
                # default, which means don't retry
    script: "chainsaw_script.sh"
    args: # Arguments are handled with the go templating engine, sending the email as data
    - "{{ .Header.To }}"
    - "{{ .Header.From }}"
    - "{{ .Header.Subject }}"
    - "{{ .Payload.Body }}"
    - "{{ .Payload.Attachments }}"
```

### What will happen next

Once started, mqil will:

1. Fetch emails that are _unseen_, then for every email it will
2. Match it with all the available mappings in no particular order. First
matching mapping will be the only one handling the email (watch out for
multiple mappings matching, be as strict as possible)
3. Create a temporary folder in which to download the attachments that
may be present in the email.
4. Execute the script defined in the mapping interpolating variables. The
list of attachments will be sent in the same variable splitting the items
with commas.
5. On exit of the script:
   - If it was successful (exit code is 0), MQIL will mark the email as deleted.
   - If it is not successful, MQIL will add one to a counter stored in the
     database file, flagging the email as _read_ so it is ignored when the
     counter exceeds the retry count (only once in the default case)
   - Attachment files are deleted
6. Wait for the next interval tick to then start the process again.
