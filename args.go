package main

import (
	"flag"
	"os"

	log "github.com/sirupsen/logrus"
)

// Args are the arguments that we can get from the commandline
type Args struct {
	Configfile      string
	DatabaseFile    string
	PollingInterval int

	MetricsPath    string
	MetricsAddress string

	NukeUnsolicited bool

	MailInbox          string
	MailServerAddress  string
	MailServerUsername string
	MailServerPassword string

	Debug  bool
	DryRun bool
}

func parseArgs() Args {
	args := Args{}

	flag.StringVar(&args.Configfile, "config", "mqil.yml", "configuration file to load mappings from")
	flag.IntVar(&args.PollingInterval, "polling-interval-seconds", 60, "number of seconds between checking email polling intervals")
	flag.StringVar(&args.MetricsPath, "metrics-path", "/metrics", "path in which prometheus metrics are presented")
	flag.StringVar(&args.MetricsAddress, "metrics-address", ":9097", "path in which prometheus metrics are presented")
	flag.StringVar(&args.DatabaseFile, "database-path", "mqil.bolt", "database filename used for email accounting")
	flag.StringVar(&args.MailInbox, "mail-inbox", "[Gmail]/All Mail", "inbox in which to look for email")
	flag.BoolVar(&args.NukeUnsolicited, "nuke-unsolicited", false, "nuke emails that don't match any filter (handle with care)")
	flag.BoolVar(&args.Debug, "debug", false, "enable debug logging")
	flag.BoolVar(&args.DryRun, "dryrun", false, "enable dryrun mode")

	flag.Parse()

	var ok bool
	if args.MailServerAddress, ok = os.LookupEnv("MQIL_SERVER_ADDRESS"); !ok {
		log.Fatalf("MQIL_SERVER_ADDRESS environment variable is required")
	}
	if args.MailServerUsername, ok = os.LookupEnv("MQIL_USERNAME"); !ok {
		log.Fatalf("MQIL_USERNAME environment variable is required")
	}
	if args.MailServerPassword, ok = os.LookupEnv("MQIL_PASSWORD"); !ok {
		log.Fatalf("MQIL_PASSWORD environment variable is required")
	}

	return args
}
