package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/yakshaving.art/mqil/internal"
	"gitlab.com/yakshaving.art/mqil/internal/config"
	"gitlab.com/yakshaving.art/mqil/internal/db"
	"gitlab.com/yakshaving.art/mqil/internal/mail"
	"gitlab.com/yakshaving.art/mqil/internal/matcher"
	"gitlab.com/yakshaving.art/mqil/internal/metrics"

	log "github.com/sirupsen/logrus"
)

func main() {
	args := parseArgs()

	if args.Debug {
		log.SetLevel(log.DebugLevel)
	}

	cnf, err := config.Load(args.Configfile)
	if err != nil {
		log.Fatalf("Failed to load configuration: %s", err)
	}

	client, err := mail.New(
		args.MailInbox,
		args.MailServerAddress,
		args.MailServerUsername,
		args.MailServerPassword)
	if err != nil {
		log.Fatalf("Failed to initialize mail client: %s", err)
	}

	d, err := db.Open(args.DatabaseFile)
	if err != nil {
		log.Fatalf("Failed to open database %s: %s", args.DatabaseFile, err)
	}
	defer d.Close()

	matchers := make([]internal.Matcher, 0)
	for _, c := range cnf.Mappings {
		matchers = append(matchers, matcher.NewRegexMatcher(c, d, args.DryRun))
	}

	if len(matchers) == 0 {
		log.Fatal("No matcher is defined, please add some")
	}

	if args.NukeUnsolicited && !args.DryRun {
		matchers = append(matchers, matcher.NewNukeAllMatcher())
	}

	go func() {
		log.Fatal(metrics.Serve(args.MetricsAddress, args.MetricsPath))
	}()

	signalCh := make(chan os.Signal, 1)
	signal.Notify(signalCh, syscall.SIGINT)

	ticker := make(chan time.Time, 1)

	go func() {
		ticker <- time.Now()
		for t := range time.Tick(time.Duration(args.PollingInterval) * time.Second) {
			ticker <- t
		}
	}()

loop:
	for {
		select {
		case <-ticker:
			ch := make(chan internal.Executor, 1)

			go func() {
				if err := client.Fetch(matchers, ch); err != nil {
					log.Errorf("Failed to fetch emails: %s", err)
				}
			}()

			for e := range ch {
				if err := e.Execute(); err != nil {
					log.Fatalf("failed to execute matched email: %s", err)
				}
			}

		case sg := <-signalCh:
			log.Infof("Received signal %s", sg)
			break loop
		}
	}
}
